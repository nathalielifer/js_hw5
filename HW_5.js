function createNewUser () {
    let firstName = prompt ("Input your name.")
    let lastName = prompt ("Input your last name.")

    const user = {
        name: firstName,
        lastName: lastName,
        birthday: prompt("Input your date of birth: dd.mm.yyyy"),
        getLogin: function () {
            return login = user.name.toLowerCase()[0] + user.lastName.toLowerCase();
        },

        getAge: function () {
            const now = new Date();
            const today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            const bDay = new Date(user.birthday.replace(/(\d{2}).(\d{2}).(\d{4})/, "$2.$1.$3"));
            const bDayThisYear = new Date(today.getFullYear(), bDay.getMonth(), bDay.getDate());

            let age = today.getFullYear() - bDay.getFullYear();

            if (today < bDayThisYear) {
                age = age - 1;
            }
            return age
        },

        getPassword: function () {
            const now = new Date ();
            let bYear = now.getFullYear() - user.getAge();
            let password = user.name.toUpperCase()[0] + user.lastName.toLowerCase() + bYear;
            return password
        }
    }

    return user;
}

const user = createNewUser ()
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());



